#ifndef DICE_H_
#define DICE_H_

#include "die.h"

class Dice{
public:
	Dice();
	int getDiceOneValue();
	int getDiceTwoValue();
	int getDiceThreeValue();
	int getDiceFourValue();
	int getDiceFiveValue();

	void rollD();
	Die die1;
	Die die2;
	Die die3;
	Die die4;
	Die die5;
	int die1local;
	int die2local;
	int die3local;
	int die4local;
	int die5local;
};


#endif
