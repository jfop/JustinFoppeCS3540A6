#include "../include/game.h"
#include "../include/scoreboard.h"
#include "../include/dice.h"
#include <iostream>

using namespace std;

Game::Game(){
	value = 0;
	Dice d;
	Scoreboard s;
}
Game::getTotalValue(){
	return s.getTotalValue();
}
void Game::play(){
	int counter = 0;
	while (counter <= 13){
		int i = 0;
		int choice = 0;
		int numOfRolls = 5;
		int temptotc1 = 0;
		d.rollD();
		cout << "Die 1: " << d.getDiceOneValue() << " Die 2: " << d.getDiceTwoValue() << " Die 3: " << d.getDiceThreeValue() << " Die 4: " << d.getDiceFourValue() << " Die 5: " << d.getDiceFiveValue() << endl;
		cout << "Which category? 1:1 2:2 3:3 4:4 5:5 6:6 7:three of a kind 8:four of a kind 9:full house 10:small straight 11: large straight 12: yahtzee 13: chance" << endl;
		cout << "14: Total 15: Play again" << endl;
		cin >> choice;


			if (choice == 1){
				counter = counter + 1;
				while (i < 3){
				d.rollD();
				if ((d.getDiceOneValue() == 1) && (numOfRolls > 0)){
					temptotc1 = temptotc1 + 1;
					numOfRolls = numOfRolls - 1;
				}
				if ((d.getDiceTwoValue() == 1) && (numOfRolls > 1)){
					temptotc1 = temptotc1 + 1;
					numOfRolls = numOfRolls - 1;
				}
				if ((d.getDiceThreeValue() == 1) && (numOfRolls > 2)){
					temptotc1 = temptotc1 + 1;
					numOfRolls = numOfRolls - 1;
				}
				if ((d.getDiceFourValue() == 1) && (numOfRolls > 3)){
					temptotc1 = temptotc1 + 1;
					numOfRolls = numOfRolls - 1;
				}
				if ((d.getDiceFiveValue() == 1) && (numOfRolls > 4)){
					temptotc1 = temptotc1 + 1;
					numOfRolls = numOfRolls - 1;
				}
				i = i + 1;

				}
				s.setOnesValue(temptotc1);
			}
			if (choice == 2){
				counter = counter + 1;
							while (i < 3){
							d.rollD();
							if ((d.getDiceOneValue() == 2) && (numOfRolls > 0)){
								temptotc1 = temptotc1 + 2;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceTwoValue() == 2) && (numOfRolls > 1)){
								temptotc1 = temptotc1 + 2;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceThreeValue() == 2) && (numOfRolls > 2)){
								temptotc1 = temptotc1 + 2;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFourValue() == 2) && (numOfRolls > 3)){
								temptotc1 = temptotc1 + 2;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFiveValue() == 2) && (numOfRolls > 4)){
								temptotc1 = temptotc1 + 2;
								numOfRolls = numOfRolls - 1;
							}
							i = i + 1;

							}
							s.setTwosValue(temptotc1);
						}
			if (choice == 3){
				counter = counter + 1;
							while (i < 3){
							d.rollD();
							if ((d.getDiceOneValue() == 3) && (numOfRolls > 0)){
								temptotc1 = temptotc1 + 3;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceTwoValue() == 3) && (numOfRolls > 1)){
								temptotc1 = temptotc1 + 3;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceThreeValue() == 3) && (numOfRolls > 2)){
								temptotc1 = temptotc1 + 3;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFourValue() == 3) && (numOfRolls > 3)){
								temptotc1 = temptotc1 + 3;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFiveValue() == 3) && (numOfRolls > 4)){
								temptotc1 = temptotc1 + 3;
								numOfRolls = numOfRolls - 1;
							}
							i = i + 1;

							}
							s.setThreesValue(temptotc1);
						}
			if (choice == 4){
				counter = counter + 1;
							while (i < 3){
							d.rollD();
							if ((d.getDiceOneValue() == 4) && (numOfRolls > 0)){
								temptotc1 = temptotc1 + 4;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceTwoValue() == 4) && (numOfRolls > 1)){
								temptotc1 = temptotc1 + 4;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceThreeValue() == 4) && (numOfRolls > 2)){
								temptotc1 = temptotc1 + 4;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFourValue() == 4) && (numOfRolls > 3)){
								temptotc1 = temptotc1 + 4;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFiveValue() == 4) && (numOfRolls > 4)){
								temptotc1 = temptotc1 + 4;
								numOfRolls = numOfRolls - 1;
							}
							i = i + 1;

							}
							s.setFoursValue(temptotc1);
						}
			if (choice == 5){
				counter = counter + 1;
							while (i < 3){
							d.rollD();
							if ((d.getDiceOneValue() == 5) && (numOfRolls > 0)){
								temptotc1 = temptotc1 + 5;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceTwoValue() == 5) && (numOfRolls > 1)){
								temptotc1 = temptotc1 + 5;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceThreeValue() == 5) && (numOfRolls > 2)){
								temptotc1 = temptotc1 + 5;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFourValue() == 5) && (numOfRolls > 3)){
								temptotc1 = temptotc1 + 5;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFiveValue() == 5) && (numOfRolls > 4)){
								temptotc1 = temptotc1 + 5;
								numOfRolls = numOfRolls - 1;
							}
							i = i + 1;

							}
							s.setFivesValue(temptotc1);
						}
			if (choice == 6){
				counter = counter + 1;
							while (i < 3){
							d.rollD();
							if ((d.getDiceOneValue() == 6) && (numOfRolls > 0)){
								temptotc1 = temptotc1 + 6;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceTwoValue() == 6) && (numOfRolls > 1)){
								temptotc1 = temptotc1 + 6;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceThreeValue() == 6) && (numOfRolls > 2)){
								temptotc1 = temptotc1 + 6;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFourValue() == 6) && (numOfRolls > 3)){
								temptotc1 = temptotc1 + 6;
								numOfRolls = numOfRolls - 1;
							}
							if ((d.getDiceFiveValue() == 6) && (numOfRolls > 4)){
								temptotc1 = temptotc1 + 6;
								numOfRolls = numOfRolls - 1;
							}
							i = i + 1;

							}
							s.setSixesValue(temptotc1);
						}
			if (choice == 14){
				cout << "Total is: " << s.getTotalValue() << endl;
			}
			if (choice == 15){
							Game gz;
							gz.play();
			}
		}
	}
