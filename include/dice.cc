#include "../include/dice.h"
#include "../include/die.h"

Dice::Dice(){
	die1local = 0;
	die2local = 0;
	die3local = 0;
	die4local = 0;
	die5local = 0;
}
int Dice::getDiceOneValue(){
	return die1local;
}
int Dice::getDiceTwoValue(){
	return die2local;
}
int Dice::getDiceThreeValue(){
	return die3local;
}
int Dice::getDiceFourValue(){
	return die4local;
}
int Dice::getDiceFiveValue(){
	return die5local;
}
void Dice::rollD(){
	die1.roll();
	die2.roll();
	die3.roll();
	die4.roll();
	die5.roll();
	die1local = die1.getTestvValue();
	die2local = die2.getTestvValue();
	die3local = die3.getTestvValue();
	die4local = die4.getTestvValue();
	die5local = die5.getTestvValue();
}
