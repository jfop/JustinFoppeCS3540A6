#include "../include/scoreboard.h"

Scoreboard::Scoreboard(){
	value = 3;

	ones = 0;
	twos = 0;
	threes = 0;
	fours = 0;
	fives = 0;
	sixes = 0;

	threeofakind = 0;
	fourofakind = 0;
	fullhouse = 0;
	smlstraight = 0;
	lrgstraight = 0;
	yahtzee = 0;
	chance = 0;

	total = 0;
	bonus = 0;
	yahtzeebonus = 0;
}

void Scoreboard::setOnesValue(int x){
	ones = x;
	total = total + x;
}
void Scoreboard::setTwosValue(int x){
	twos = x;
	total = total + x;
}
void Scoreboard::setThreesValue(int x){
	threes = x;
	total = total + x;
}
void Scoreboard::setFoursValue(int x){
	fours = x;
	total = total + x;
}
void Scoreboard::setFivesValue(int x){
	fives = x;
	total = total + x;
}
void Scoreboard::setSixesValue(int x){
	sixes = x;
	total = total + x;
}
void Scoreboard::setThreeOfaKindValue(int x){
	threeofakind = x;
	total = total + x;
}
void Scoreboard::setFourOfaKindValue(int x){
	fourofakind = x;
	total = total + x;
}
void Scoreboard::setFullHouseValue(int x){
	fullhouse = x;
	total = total + x;
}
void Scoreboard::setSmlStraightValue(int x){
	smlstraight = x;
	total = total + x;
}
void Scoreboard::setLrgStraightValue(int x){
	lrgstraight = x;
	total = total + x;
}
void Scoreboard::setYahtzeeValue(int x){
	yahtzee = x;
	total = total + x;
}
void Scoreboard::setChanceValue(int x){
	chance = x;
	total = total + x;
}
void Scoreboard::setYahtzeeBonusValue(int x){
	yahtzeebonus = x;
	total = total + x;
}
void Scoreboard::setBonusValue(bool x){
	if (x == true){
		bonus = 35;
		total = total + 35;
	}
}


int Scoreboard::getOnesValue(){
	return ones;
}
int Scoreboard::getTwosValue(){
	return twos;
}
int Scoreboard::getThreesValue(){
	return threes;
}
int Scoreboard::getFoursValue(){
	return fours;
}
int Scoreboard::getFivesValue(){
	return fives;
}
int Scoreboard::getSixesValue(){
	return sixes;
}
int Scoreboard::getThreeOfaKindValue(){
	return threeofakind;
}
int Scoreboard::getFourOfaKindValue(){
	return fourofakind;
}
int Scoreboard::getFullHouseValue(){
	return fullhouse;
}
int Scoreboard::getSmlStraightValue(){
	return smlstraight;
}
int Scoreboard::getLrgStraightValue(){
	return lrgstraight;
}
int Scoreboard::getYahtzeeValue(){
	return yahtzee;
}
int Scoreboard::getChanceValue(){
	return chance;
}
int Scoreboard::getTotalValue(){
	return total;
}
int Scoreboard::getBonusValue(){
	return bonus;
}
int Scoreboard::getYahtzeeBonusValue(){
	return yahtzeebonus;
}

int Scoreboard::getValue(){
	return value;
}
