#include <cstdlib>
#include <ctime>
#include "../include/die.h"

Die::Die(){
	value = 0;

	testv = 22;

	unsigned x = time(0);
	std::srand(x);
}
void Die::roll(){
	testv = (rand() % 6 - 1 + 1) + 1;
}
int Die::getTestvValue(){
	return testv;
}
int Die::getValue(){
	return value;
}
