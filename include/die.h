#ifndef DIE_H_
#define DIE_H_

class Die{
public:
	Die();
	void roll();
	int getTestvValue();
	int getValue();
private:
	int value;
	int testv;
};


#endif
