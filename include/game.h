#ifndef GAME_H_
#define GAME_H_
#include "scoreboard.h"
#include "dice.h"
class Game{
public:
	Game();
	int value;
	int getTotalValue();
	void play();

	Dice d;
	Scoreboard s;
};

#endif
