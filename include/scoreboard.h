#ifndef SCOREBOARD_H_
#define SCOREBOARD_H_

class Scoreboard {
public:
	Scoreboard();
	int getOnesValue();
	int getTwosValue();
	int getThreesValue();
	int getFoursValue();
	int getFivesValue();
	int getSixesValue();
	int getThreeOfaKindValue();
	int getFourOfaKindValue();
	int getFullHouseValue();
	int getSmlStraightValue();
	int getLrgStraightValue();
	int getYahtzeeValue();
	int getChanceValue();
	int getValue();

	int getBonusValue();
	int getYahtzeeBonusValue();
	int getTotalValue();

	void setOnesValue(int);
	void setTwosValue(int);
	void setThreesValue(int);
	void setFoursValue(int);
	void setFivesValue(int);
	void setSixesValue(int);
	void setThreeOfaKindValue(int);
	void setFourOfaKindValue(int);
	void setFullHouseValue(int);
	void setSmlStraightValue(int);
	void setLrgStraightValue(int);
	void setYahtzeeValue(int);
	void setChanceValue(int);


	void setBonusValue(bool);
	void setYahtzeeBonusValue(int);
private:
	int value;

	int ones;
	int twos;
	int threes;
	int fours;
	int fives;
	int sixes;

	int threeofakind;
	int fourofakind;
	int fullhouse;
	int smlstraight;
	int lrgstraight;
	int yahtzee;
	int chance;

	int total;
	int bonus;
	int yahtzeebonus;
};

#endif
