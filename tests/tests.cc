#define CATCH_CONFIG_MAIN
#include "../include/catch.hpp"
#include "../include/scoreboard.h"
#include "../include/die.h"
#include "../include/dice.h"
#include "../include/game.h"

#include <iostream>
using namespace std;

//scoreboard tests
TEST_CASE("Scoreboard creation", "[SCOREBOARD]"){
	Scoreboard s;
	REQUIRE(s.getValue() == 3);
}
TEST_CASE("Scoreboard ones set","[ONES]"){
	Scoreboard s;
	s.setOnesValue(2);
	REQUIRE(s.getOnesValue() == 2);
}
TEST_CASE("Scoreboard twos set", "[TWOS]"){
	Scoreboard s;
	s.setTwosValue(4);
	REQUIRE(s.getTwosValue() == 4);
}
TEST_CASE("Scoreboard threes set", "[THREES]"){
	Scoreboard s;
	s.setThreesValue(6);
	REQUIRE(s.getThreesValue() == 6);
}
TEST_CASE("Scoreboard fours set", "[FOURS]"){
	Scoreboard s;
	s.setFoursValue(1);
	REQUIRE(s.getFoursValue() == 1);
}
TEST_CASE("Scoreboard fives set", "[FIVES]"){
	Scoreboard s;
	s.setFivesValue(5);
	REQUIRE(s.getFivesValue() == 5);
}
TEST_CASE("Scoreboard sixes set", "[SIXES]"){
	Scoreboard s;
	s.setSixesValue(4);
	REQUIRE(s.getSixesValue() == 4);
}
TEST_CASE("Scoreboard three of a kind set", "[THREEOFKIND]"){
	Scoreboard s;
	s.setThreeOfaKindValue(8);
	REQUIRE(s.getThreeOfaKindValue() == 8);
}
TEST_CASE("Scoreboard four of a kind set", "[FOUROFKIND]"){
	Scoreboard s;
	s.setFourOfaKindValue(8);
	REQUIRE(s.getFourOfaKindValue() == 8);
}
TEST_CASE("Scoreboard full house set", "[FULLHOUSE]"){
	Scoreboard s;
	s.setFullHouseValue(8);
	REQUIRE(s.getFullHouseValue() == 8);
}
TEST_CASE("Scoreboard small straight set", "[SMLSTRAIGHT]"){
	Scoreboard s;
	s.setSmlStraightValue(7);
	REQUIRE(s.getSmlStraightValue() == 7);
}
TEST_CASE("Scoreboard large straight set", "[LRGSTRAIGHT]"){
	Scoreboard s;
	s.setLrgStraightValue(7);
	REQUIRE(s.getLrgStraightValue() == 7);
}
TEST_CASE("Scoreboard yahtzee set", "[YAHTZEE]"){
	Scoreboard s;
	s.setYahtzeeValue(33);
	REQUIRE(s.getYahtzeeValue() == 33);
}
TEST_CASE("Scoreboard chance set", "[CHANCE]"){
	Scoreboard s;
	s.setChanceValue(3);
	REQUIRE(s.getChanceValue() == 3);
}
TEST_CASE("Scoreboard total calculated", "[TOTAL]"){
	Scoreboard s;
	s.setOnesValue(5);
	s.setTwosValue(4);
	s.setThreesValue(100);
	REQUIRE(s.getTotalValue() == 109);
}
TEST_CASE("Scoreboard bonus calculated", "[BONUS]"){
	Scoreboard s;
	s.setBonusValue(true);
	REQUIRE(s.getBonusValue() == 35);
}
TEST_CASE("Scoreboard yahtzee bonus calculated", "[YAHTZEEBONUS]"){
	Scoreboard s;
	s.setYahtzeeBonusValue(4);
	REQUIRE(s.getYahtzeeBonusValue() == 4);
}

// Die tests

TEST_CASE("Object single Die created", "[DIE]"){
	Die d;
	REQUIRE(d.getValue() == 0);
}
TEST_CASE("Object single Die rolled randomly(1-6)", "[DIE]"){
	Die d;
	d.roll();
	REQUIRE(d.getTestvValue() > 0);
	REQUIRE(d.getTestvValue() < 7);
}

// Dice tests (rolls 5 die)



TEST_CASE("A dice object containing 5 die's created", "[DICE]"){
	Dice di;
	REQUIRE(di.getDiceOneValue() == 0);
	REQUIRE(di.getDiceTwoValue() == 0);
	REQUIRE(di.getDiceThreeValue() == 0);
	REQUIRE(di.getDiceFourValue() == 0);
	REQUIRE(di.getDiceFiveValue() == 0);
}
TEST_CASE("Rolls a set of 5 die's", "[DICE]"){
	Dice dice;
	dice.rollD();
	REQUIRE(dice.getDiceOneValue() > 0);
	REQUIRE(dice.getDiceOneValue() < 7);

	REQUIRE(dice.getDiceTwoValue() > 0);
	REQUIRE(dice.getDiceTwoValue() < 7);

	REQUIRE(dice.getDiceThreeValue() > 0);
	REQUIRE(dice.getDiceThreeValue() < 7);

	REQUIRE(dice.getDiceFourValue() > 0);
	REQUIRE(dice.getDiceFourValue() < 7);

	REQUIRE(dice.getDiceFiveValue() > 0);
	REQUIRE(dice.getDiceFiveValue() < 7);
}

// Game tests


TEST_CASE("Game object created", "[GAME]"){
	Game g;
	REQUIRE(g.value == 0);
}
TEST_CASE("Game able to play", "[GAME]"){
	Game g;
	cout << "TEST GAME TEST GAME TEST GAME" << endl;
	g.play();
	REQUIRE(g.getTotalValue() != 0);
}
